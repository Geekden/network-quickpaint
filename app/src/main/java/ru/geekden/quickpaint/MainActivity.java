package ru.geekden.quickpaint;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.Toolbar.OnMenuItemClickListener;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import petrov.kristiyan.colorpicker.ColorPicker;
import yogesh.firzen.filelister.FileListerDialog;
import yogesh.firzen.filelister.OnFileSelectedListener;
import yogesh.firzen.filelister.FileListerDialog.Companion;

public class MainActivity extends AppCompatActivity {
    public static String SERVER_URL = "http://192.168.0.9:3000";
    public static String authToken = "";
    public static String profileName = "";
    private String name = "New Paint";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Top navigation
        Toolbar toolbarView = (Toolbar) findViewById(R.id.topBarView);
        toolbarView.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_new:
                        newPaint();
                        break;
                    case R.id.action_save:
                        savePaint();
                        break;
                    case R.id.action_account:
                        goToAccountPage();
                        break;
                }
                return true;
            }
        });

        // Bottom navigation
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener( new OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_pencil:
                        DrawingView.setPencil();
                        break;
                    case R.id.action_brush:
                        DrawingView.setBrush();
                        break;
                    case R.id.action_rubber:
                        DrawingView.setRubber();
                        break;
                    case R.id.action_color:
                        newColor();
                        break;
                }
                return true;
            }
        });
    }

    private void goToAccountPage() {
        if (MainActivity.authToken != "") {
            Intent myIntent = new Intent(MainActivity.this, ProfileActivity.class);
            MainActivity.this.startActivity(myIntent);
        } else {
            Intent myIntent = new Intent(MainActivity.this, RegistrationActivity.class);
            MainActivity.this.startActivity(myIntent);
        }
    }

    private void savePaint() {
        String uuid = UUID.randomUUID().toString();
        String folderPath = Environment.getExternalStorageDirectory() + "/Download/";
        String fileName =  uuid + ".jpg";
        String path = folderPath + fileName;
        DrawingView drawingView = (DrawingView) findViewById(R.id.paintView);
        drawingView.savePaint(path);
        uploadPaint(folderPath, fileName);
        Toast.makeText(this, "Рисунок успешно загружен", Toast.LENGTH_SHORT).show();
    }

    public void uploadPaint(String folderPath, String fileName) {
        final String uploadFilePath = folderPath;
        final String uploadFileName = fileName;
        final String sourceFileUri = uploadFilePath + uploadFileName;
        final String upLoadServerUri = SERVER_URL + "/api/upload";


        new Thread(new Runnable() {
            public void run() {
                uploadFile(uploadFilePath, uploadFileName, sourceFileUri,
                        upLoadServerUri, name, "Made with QuickPaint");
            }
        }).start();

    }

    public int uploadFile(final String uploadFilePath, final String uploadFileName,
                          String sourceFileUri, String upLoadServerUri, String title, String body) {

        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "qwerty";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {

            Log.e("uploadFile", "Source File not exist :"
                    + uploadFilePath + "" + uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
                    Log.i(MainActivity.class.getSimpleName(), "OUTPUT Upload file: " + uploadFilePath + "" + uploadFileName);
                }
            });

            return 0;

        } else {
            int serverResponseCode = 0;
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(upLoadServerUri);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "*/*");
                conn.setRequestProperty("token", authToken);
                conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"file\"; filename=\"" + uploadFileName + "\"");
                dos.writeBytes(lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"title\"");
                dos.writeBytes(lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(title);
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"body\"");
                dos.writeBytes(lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(body);
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens);
                dos.writeBytes(lineEnd);


                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i(MainActivity.class.getSimpleName(), "OUTPUT HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 201) {
                    Log.i(MainActivity.class.getSimpleName(), "OUTPUT File Upload Completed: " + uploadFileName);
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {

                ex.printStackTrace();

                Log.i(MainActivity.class.getSimpleName(), "OUTPUT File Upload error: " + ex.getMessage(), ex);
                Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {

                e.printStackTrace();
                Log.i(MainActivity.class.getSimpleName(), "OUTPUT File Upload Exception : "
                        + e.getMessage(), e);
                Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
            return serverResponseCode;

        } // End else block
    }

    private void newPaint() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toolbar toolbarView = (Toolbar) findViewById(R.id.topBarView);
                name = input.getText().toString();
                toolbarView.setTitle(name);
                DrawingView.clean();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void newColor() {
        ColorPicker colorPicker = new ColorPicker(MainActivity.this);
        colorPicker.show();
        colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
            @Override
            public void onChooseColor(int position, int color) {
                DrawingView.setColor(color);

                BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
                switch (DrawingView.getLastTool()) {
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.action_pencil);
                        DrawingView.setPencil();
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.action_brush);
                        DrawingView.setBrush();
                        break;
                    case 3:
                        bottomNavigationView.setSelectedItemId(R.id.action_rubber);
                        DrawingView.setRubber();
                        break;
                }
            }

            @Override
            public void onCancel(){
                // put code
            }
        });
    }

    static class Request {
        public URL url;
        public String method;
        public HashMap<String, String> props;
        public JSONObject data;

        public Request(URL url, String method, HashMap<String, String> props, JSONObject data) {
            this.url = url;
            this.method = method;
            this.props = props;
            this.data = data;
        }
    }

    static class NetworkTask extends AsyncTask<Request, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Request[] objects) {
            return connectAndSend(objects[0]);
        }
    }

    private static JSONObject connectAndSend(Request request) {
        try {
            HttpURLConnection con = (HttpURLConnection) request.url.openConnection();
            con.setRequestMethod(request.method);
            for (Map.Entry<String, String> entry : request.props.entrySet()) {
                con.setRequestProperty(entry.getKey(), entry.getValue());
            }

            if (request.data.length() > 0) {
                OutputStream os = con.getOutputStream();
                os.write(request.data.toString().getBytes());
            }
            InputStream is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder builder = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            String result = builder.toString();
            if (result.startsWith("[")) {
                result = "{ \"uploads\": " + result + "}";
            }
            return new JSONObject(result);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}