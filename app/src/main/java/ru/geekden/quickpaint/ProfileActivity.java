package ru.geekden.quickpaint;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class ProfileActivity extends AppCompatActivity {
    private ListView list;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private JSONArray files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Top navigation
        Toolbar toolbarView = (Toolbar) findViewById(R.id.profileNav);

        toolbarView.setTitle("Профиль " + MainActivity.profileName);

        toolbarView.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_paint_page:
                        goToDrawingPage();
                        break;
                    case R.id.action_logout:
                        onLogout();
                        break;
                }
                return true;
            }
        });

        list = (ListView) findViewById(R.id.filesListElement);
        arrayList = new ArrayList<String>();

        // Adapter: You need three parameters 'the context, id of the layout (it will be where the data is shown),
        // and the array that contains the data
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        list.setAdapter(adapter);
        getFiles();

        if (files != null) {
            for (int i=0;i<files.length();i++){
                try {
                    arrayList.add(files.getJSONObject(i).getString("title"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void goToDrawingPage() {
        Intent myIntent = new Intent(ProfileActivity.this, MainActivity.class);
        ProfileActivity.this.startActivity(myIntent);
    }

    public void onLogout() {
        URL url = null;
        try {
            url = new URL(MainActivity.SERVER_URL + "/api/logout/" + MainActivity.profileName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        MainActivity.NetworkTask t = new MainActivity.NetworkTask();

        try {
            JSONObject data = new JSONObject();
            HashMap<String, String> props = new HashMap<>();
            props.put("Content-Type", "application/json");
            props.put("token", MainActivity.authToken);
            MainActivity.Request request = new MainActivity.Request(url, "GET", props, data);

            t.execute(request);

            JSONObject result = t.get();
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Result: " + result);
            if (result != null) {
                MainActivity.authToken = "";
                MainActivity.profileName = "";
                goToDrawingPage();
            } else {
                Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        }
    }

    public void onDeleteAccount(View view) {
        URL url = null;
        try {
            url = new URL(MainActivity.SERVER_URL + "/api/deleteaccount/" + MainActivity.profileName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        MainActivity.NetworkTask t = new MainActivity.NetworkTask();

        try {
            JSONObject data = new JSONObject();
            HashMap<String, String> props = new HashMap<>();
            props.put("Content-Type", "application/json");
            props.put("token", MainActivity.authToken);
            MainActivity.Request request = new MainActivity.Request(url, "POST", props, data);

            t.execute(request);

            JSONObject result = t.get();
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Result: " + result);
            MainActivity.authToken = "";
            MainActivity.profileName = "";
            goToDrawingPage();
        } catch (ExecutionException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        }
    }

    public void getFiles() {
        URL url = null;
        try {
            url = new URL(MainActivity.SERVER_URL + "/api/uploads/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        MainActivity.NetworkTask t = new MainActivity.NetworkTask();

        try {
            JSONObject data = new JSONObject();
            HashMap<String, String> props = new HashMap<>();
            props.put("Content-Type", "application/json");
            props.put("token", MainActivity.authToken);
            MainActivity.Request request = new MainActivity.Request(url, "GET", props, data);

            t.execute(request);

            JSONObject result = t.get();
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Result: " + result);
            if (result != null) {
                files = result.getJSONArray("uploads");
            } else {
                Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_SHORT).show();
        }
    }
}