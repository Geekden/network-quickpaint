package ru.geekden.quickpaint;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import ru.geekden.quickpaint.MainActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Top navigation
        Toolbar toolbarView = (Toolbar) findViewById(R.id.loginNav);
        toolbarView.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_paint_page:
                        goToDrawingPage();
                        break;
                }
                return true;
            }
        });
    }

    private void goToDrawingPage() {
        Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    public void goToRegistrationPage(View v) {
        Intent myIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    private void goToProfilePage() {
        Intent myIntent = new Intent(LoginActivity.this, ProfileActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    public void onLogin(View view) {
        EditText loginInput = (EditText)findViewById(R.id.loginLoginInut);
        String login = loginInput.getText().toString();

        EditText passwordInput = (EditText)findViewById(R.id.loginPasswordInput);
        String password = passwordInput.getText().toString();

        TextView errorField = (TextView)findViewById(R.id.loginError);

        URL url = null;
        try {
            url = new URL(MainActivity.SERVER_URL + "/api/login");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        MainActivity.NetworkTask t = new MainActivity.NetworkTask();

        try {
            JSONObject data = new JSONObject();
            data.put("login", login);
            data.put("password", password);
            HashMap<String, String> props = new HashMap<>();
            props.put("Content-Type", "application/json");
            MainActivity.Request request = new MainActivity.Request(url, "POST", props, data);

            t.execute(request);

            JSONObject result = t.get();
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Result: " + result);
            if (result != null) {
                MainActivity.authToken = result.getString("token");
                MainActivity.profileName = login;
                goToProfilePage();
            } else {
                errorField.setText("Ошибка сервера");
            }
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Token: " + MainActivity.authToken);
        } catch (ExecutionException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
        } catch (InterruptedException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
        } catch (JSONException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
        }
    }
}