package ru.geekden.quickpaint;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import ru.geekden.quickpaint.MainActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class RegistrationActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // Top navigation
        Toolbar toolbarView = (Toolbar) findViewById(R.id.registrationNav);
        toolbarView.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_paint_page:
                        goToDrawingPage();
                        break;
                }
                return true;
            }
        });
    }

    private void goToDrawingPage() {
        Intent myIntent = new Intent(RegistrationActivity.this, MainActivity.class);
        RegistrationActivity.this.startActivity(myIntent);
    }

    public void goToLoginPage(View v) {
        Intent myIntent = new Intent(RegistrationActivity.this, LoginActivity.class);
        RegistrationActivity.this.startActivity(myIntent);
    }

    private void goToProfilePage() {
        Intent myIntent = new Intent(RegistrationActivity.this, ProfileActivity.class);
        RegistrationActivity.this.startActivity(myIntent);
    }

    public void onSignup(View view) {
        EditText loginInput = (EditText)findViewById(R.id.registrationLoginInut);
        String login = loginInput.getText().toString();

        EditText passwordInput = (EditText)findViewById(R.id.registrationPasswordInput);
        String password = passwordInput.getText().toString();

        TextView errorField = (TextView)findViewById(R.id.registrationError);

        URL url = null;
        try {
            url = new URL(MainActivity.SERVER_URL + "/api/signup");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        MainActivity.NetworkTask t = new MainActivity.NetworkTask();

        try {
            JSONObject data = new JSONObject();
            data.put("login", login);
            data.put("password", password);
            HashMap<String, String> props = new HashMap<>();
            props.put("Content-Type", "application/json");
            MainActivity.Request request = new MainActivity.Request(url, "POST", props, data);

            t.execute(request);

            JSONObject result = t.get();
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Result: " + result);
            if (result != null) {
                MainActivity.profileName = result.getString("login");
            } else {
                errorField.setText("Ошибка сервера");
                return;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
            return;
        } catch (JSONException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
            return;
        }


        try {
            url = new URL(MainActivity.SERVER_URL + "/api/login");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        t = new MainActivity.NetworkTask();
        try {
            JSONObject data = new JSONObject();
            data.put("login", login);
            data.put("password", password);
            HashMap<String, String> props = new HashMap<>();
            props.put("Content-Type", "application/json");
            MainActivity.Request request = new MainActivity.Request(url, "POST", props, data);

            t.execute(request);

            JSONObject result = t.get();
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Result: " + result);
            if (result != null) {
                MainActivity.authToken = result.getString("token");
                goToProfilePage();
            } else {
                errorField.setText("Ошибка сервера");
            }
            Log.i(MainActivity.class.getSimpleName(), "OUTPUT Token: " + MainActivity.authToken);
        } catch (ExecutionException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
        } catch (InterruptedException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
        } catch (JSONException e) {
            e.printStackTrace();
            errorField.setText("Ошибка сервера");
        }
    }
}