package ru.geekden.quickpaint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.widget.Toolbar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class DrawingView extends View {
    private static Canvas current_canvas;

    public static int lastTool = 1;

    private static int defaultColor = Color.BLACK;

    private static int paintColor = Color.BLACK;
    private static int paintWidth = 5;

    private static ArrayList<Path> paths = new ArrayList<Path>();
    private static ArrayList<Paint> paints = new ArrayList<Paint>();

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDrawingCacheEnabled(true);

        DrawingView.addPath();
    }

    public void savePaint(String folderPath){
        try {
            getDrawingCache().compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(new File(folderPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setStrokeWith(int width){
        paintWidth = width;
    }

    public static void setColor(int color) {
        defaultColor = color;
        paintColor = color;
    }

    public static int getLastTool() {
        return lastTool;
    }

    public static void setPencil() {
        lastTool = 1;
        paintColor = defaultColor;
        DrawingView.setStrokeWith(5);
    }

    public static void setBrush() {
        lastTool = 2;
        paintColor = defaultColor;
        DrawingView.setStrokeWith(30);
    }

    public static void setRubber() {
        lastTool = 3;
        defaultColor = paintColor;
        paintColor = Color.WHITE;
        DrawingView.setStrokeWith(30);
    }

    public static void clean() {
        paths.clear();
        paints.clear();

        Path path = DrawingView.addPath();
        Paint drawPaint = new Paint();

        current_canvas.drawPath(path, drawPaint);
    }

    private static Path addPath() {
        Path path = new Path();
        paths.add(path);

        Paint drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(paintWidth);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        paints.add(drawPaint);

        return path;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        current_canvas = canvas;

        for (int i = 0; i < paths.size(); i++) {
            Path path = paths.get(i);
            Paint drawPaint = paints.get(i);

            canvas.drawPath(path, drawPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Path path = paths.get(paths.size() - 1);
        float pointX = event.getX();
        float pointY = event.getY();
        // Checks for the event that occurs
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Path new_path = DrawingView.addPath();
                new_path.moveTo(pointX, pointY);
                return true;
            case MotionEvent.ACTION_MOVE:
                path = paths.get(paths.size() - 1);
                path.lineTo(pointX, pointY);
                break;
            case MotionEvent.ACTION_UP:
                path = paths.get(paths.size() - 1);
                return true;
            default:
                return false;
        }
        // Force a view to draw again
        postInvalidate();
        return true;
    }
}